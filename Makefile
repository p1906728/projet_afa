CC		  := g++
CCOPTIONS := -Wall -Wextra -std=c++17 -ggdb

INCLUDE	:= include
LIB		:= lib

LIBRARIES	:= -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio
EXECUTABLE = ./bin/main ./bin/txt


all: $(EXECUTABLE)


./bin/main : ./obj/main.o ./obj/jeu.o ./obj/input.o ./obj/map.o ./obj/gestionmenu.o ./obj/menu.o ./obj/music.o
	$(CC) $(CCOPTIONS) -I$(INCLUDE) -L$(LIB) ./obj/main.o ./obj/jeu.o ./obj/input.o ./obj/map.o ./obj/gestionmenu.o ./obj/menu.o ./obj/music.o -o ./bin/main $(LIBRARIES)

./obj/main.o : ./src/main.cpp ./src/jeu.h ./src/input.h ./src/gestionmenu.h ./src/menu.h ./src/music.h ./src/map.h
	$(CC) $(CCOPTIONS) -c ./src/main.cpp -o ./obj/main.o

./obj/jeu.o : ./src/jeu.cpp ./src/jeu.h ./src/input.h ./src/gestionmenu.h ./src/menu.h ./src/music.h ./src/map.h
	$(CC) $(CCOPTIONS) -c ./src/jeu.cpp -o ./obj/jeu.o

./obj/input.o : ./src/input.cpp ./src/input.h
	$(CC) $(CCOPTIONS) -c ./src/input.cpp -o ./obj/input.o


./bin/txt : ./obj/winTxt.o ./obj/txtJeu.o ./obj/Player.o ./obj/Map.o ./obj/Game.o
	$(CC) $(CCOPTIONS) ./obj/winTxt.o ./obj/txtJeu.o ./obj/Player.o ./obj/Map.o ./obj/Game.o -o ./bin/txt

./obj/winTxt.o : ./txt/winTxt.cpp ./txt/winTxt.h
	$(CC) $(CCOPTIONS) -c ./txt/winTxt.cpp -o ./obj/winTxt.o

./obj/txtJeu.o : ./txt/txtJeu.cpp ./txt/txtJeu.h
	$(CC) $(CCOPTIONS) -c ./txt/txtJeu.cpp -o ./obj/txtJeu.o

./obj/Player.o : ./txt/Player.cpp ./txt/Player.h
	$(CC) $(CCOPTIONS) -c ./txt/Player.cpp -o ./obj/Player.o

./obj/Map.o : ./txt/Map.cpp ./txt/Map.h
	$(CC) $(CCOPTIONS) -c ./txt/Map.cpp -o ./obj/Map.o

./obj/Game.o : ./txt/Game.cpp ./txt/Game.h
	$(CC) $(CCOPTIONS) -c ./txt/Game.cpp -o ./obj/Game.o

./obj/map.o : ./src/map.cpp ./src/map.h
	$(CC) $(CCOPTIONS) -c ./src/map.cpp -o ./obj/map.o

./obj/gestionmenu.o : ./src/gestionmenu.cpp ./src/music.h ./src/menu.h ./src/map.h
	$(CC) $(CCOPTIONS) -c ./src/gestionmenu.cpp -o ./obj/gestionmenu.o

./obj/menu.o : ./src/menu.cpp ./src/menu.h
	$(CC) $(CCOPTIONS) -c ./src/menu.cpp -o ./obj/menu.o

./obj/music.o : ./src/music.cpp ./src/music.h
	$(CC) $(CCOPTIONS) -c ./src/music.cpp -o ./obj/music.o



clean:
	-rm ./obj/* | rm ./bin/*
