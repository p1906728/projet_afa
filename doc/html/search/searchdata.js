var indexSectionsWithContent =
{
  0: "abcdeghijklmnrstuw",
  1: "ghijm",
  2: "gijm",
  3: "acdeghiklmst",
  4: "abcghmnrsw",
  5: "d",
  6: "adlru"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Classes",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Énumérations",
  6: "Valeurs énumérées"
};

