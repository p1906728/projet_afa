
#include <iostream>
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif // WIN32
#include "winTxt.h"
#include "Game.h"


void move_player(WinTXT &win, Game &g) {
    char c = win.getCh();

    g.touche(c);
}


void display_text(WinTXT &win, const Game &g){
   
       for(int i = 0; i < g.map.get_dimx();i++){
           for(int j = 0; j < g.map.get_dimy(); j++) {
            switch (g.map.get_posMap(i,j))  {
                   case 0:
                            win.print(i,j,'.');
                            break;
                    case 1:   
                            win.print(i,j,'X');
                            break;
            default:
                    break;
        }

        }
    }
    win.print(g.p1.getposx(),g.p1.getposy(), 'P');
    win.print(g.p2.getposx(),g.p2.getposy(), 'O');
    win.draw();
}

int main() {
	// Creation d'une nouvelle fenetre en mode texte
	// => fenetre de dimension et position (WIDTH,HEIGHT,STARTX,STARTY)
    Game g;
    int i = 0;
    WinTXT win (g.getConsMap().get_dimx(),g.getConsMap().get_dimy());
	bool ok = true;
	int c;

	do {
        win.clear();
        move_player(win,g);
	    display_text(win,g);
        //win.print(i,10 , 'P');
        i++;
        usleep(100000);
        c= win.getCh();
        if( c == 'g')
            ok = false;

	} while (ok);

}
