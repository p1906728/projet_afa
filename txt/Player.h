
#ifndef PLAYER_H
#define PLAYER_H

#include "Map.h"

class Player{
    int posx,posy;

    public:

        Player();

        void left(Mapping& m);
        void right( Mapping &m);
        void up(Mapping & m);
        void down(Mapping & m);
        int getposx()const;
        int getposy()const;
        int takeposx(int x);
        int takeposy(int y);
        int takenewpositiony(int y);
        int takenewpositionx(int x);

};

#endif