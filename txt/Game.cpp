#include "Game.h"

Game::Game () : map(), p1() {

}

void Game::touche (const char touche) {
    
    
    switch(touche) {
		case 'q' :
				p1.left(map);
				break;
		case 'd' :
				p1.right(map);
				break;
		case 'z' :
				p1.up(map);
				break;
		case 's' :
				p1.down(map);
				break;
	}

	 switch(touche) {
		case 'j' :
				p2.left(map);
				break;
		case 'l' :
				p2.right(map);
				break;
		case 'i' :
				p2.up(map);
				break;
		case 'k' :
				p2.down(map);
				break;
	}
}

const Mapping& Game::getConsMap()const {
    return map;
}

const Player& Game::getConstPlayer () const {
	return p1;
}