

#ifndef GAME_H
#define GAME_H


#include "Player.h"
#include "Map.h"

class Game{

    public :

        Mapping map;
        Player p1;
        Player p2;

    public:
            Game();
            const Mapping& getConsMap () const;
            const Player& getConstPlayer () const;  
            void touche(const char touche);    
               
};

#endif