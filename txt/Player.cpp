#include "Player.h"
#include <iostream>

using namespace std;
Player::Player (){
    cout<<"Player en x :"<<endl;
    cin>>posx;
    cout<<"Player en y :"<<endl;
    cin>>posy;

}

void Player::down(Mapping & m) {
    int y = posy ;
    y++;
    if(m.Collision(posx,y)) posy++;
    
}

void Player::up( Mapping & m){
    int y = posy ;
    y--;
    if(m.Collision(posx,y)) posy--;
   
}

void Player::left( Mapping  &m){
    int x = posx ;
    x--;
    if(m.Collision(x,posy)) posx--;
    
}


void Player::right( Mapping & m){
    int x = posx ;
    x++;
    if(m.Collision(x,posy))  posx++;
  
}
int Player::getposx () const{

    return(posx);

}

int Player::getposy() const {
    return(posy);
}

int Player::takenewpositionx(int x){
    posx = x;
    return(posx);
}

int Player::takenewpositiony(int y){
    posy = y;
    return(posy);
}