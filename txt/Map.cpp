#include "Map.h"

#include <unistd.h>
#include <iostream>

using namespace std;

Mapping::Mapping () {
   dimx = 20;
   dimy = 15;


    map_2d = new int*[dimx];
    for(int i = 0;i < dimx;i++)
        map_2d[i] = new int [dimy];



    for(int i = 0; i < dimx; i++) {
        for(int j = 0; j < dimy; j++)
            if( i == 0 || i == dimx - 1 || j == 0 || j == dimy - 1) {
                map_2d[i][j]= 1;
            } else{
                map_2d[i][j] = 0;
            }
    }
}
int ** Mapping::get_map()const{
    return(map_2d);

}
void Mapping::display_map()const{
        for(int i = 0; i < dimx;i++){
           for(int j = 0; j < dimy; j++) {
            switch (map_2d[i][j])  {
                   case 0:
                            cout<<" . ";
                            break;

                    case 1:
                            cout <<" X ";
                            break;
                    case 2:
                            cout<<" P1";
                            break;
                    case 3:
                            cout<<" P2";
                            break;
            default:
                    break;
        }

        }
        cout<<endl;
    }
}


bool Mapping::Collision (int x, int y){
    if((x > 0 && x < dimx && y > 0 && y < dimy ) && map_2d[x][y] == 0) {
    
        return(true);
    }else
        return(false);
}

int Mapping::get_dimx () const{
    return(dimx);
}

int Mapping::get_dimy()const {
    return(dimy);
}

int Mapping::get_posMap(int x, int y) const{
    return(map_2d[x][y]);
}




