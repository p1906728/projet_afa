


#ifndef MAP_H
#define MAP_H


class Mapping{
    
    int dimx;
    int dimy;
    int **map_2d;

    public :

         Mapping();
         bool Collision(int x, int y);
         void display_map()const;
         int get_dimx()const;
         int get_dimy()const;
         int** get_map()const;
         int  get_posMap(int x, int y)const;

};
#endif