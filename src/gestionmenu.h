
/**
 * @file gestionmenu.h
 * @author AFA
 * @brief 
 * @version 0.1
 * @date 2022-05-16
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "map.h"
#include "menu.h"
#include "music.h"



class Gmap {

  public:
    /**
     * @brief retourne un tableau de string
     * 
     * @param s 
     * @param delim 
     * @return vector<string> 
     */
    vector<string> explode(string const &s, char delim);

    /**
     * @brief transforme un int en char
     * 
     */

    void Transformchar_in_int();

    /**
     * @brief gere le choix de la map
     * 
     * @param map 
     * @param choisis 
     * @return int 
     */
    int map_choisis (Map &map,int choisis);
    
    /**
     * @brief gere les evenements de la map
     * 
     * @param window 
     * @param map 
     */
    void evenement_map(RenderWindow &window, Map &map);

    /**
     * @brief gere la gestion des boutons 
     * 
     * @param window 
     * @param menu 
     * @param song 
     * @param choice_menu 
     */
    void gestionkey(RenderWindow &window,menu &menu,music &song ,bool &choice_menu);

    /**
     * @brief affiche le choix de la map
     * 
     * @param choice 
     */
    void appelation_map(int choice);

    /**
     * @brief gere les evenement du menu d acceuil
     * 
     * @param window 
     * @param menu 
     * @param sprite 
     * @param song 
     * @param choice_menu 
     */
    void evenement_menu(RenderWindow &window, menu &menu,Sprite &sprite ,music &song, bool &choice_menu);

    /**
     * @brief affiche le choix du menu
     * 
     * @return int 
     */
    int appelation_menu();
};

