/**
 * @file gestionmenu.cpp
 * @author AFA
 * @brief 
 * @version 0.1
 * @date 2022-05-16
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include  "gestionmenu.h"
int mapload[450];

/**
 * @brief retourne le tableau de string en enlevant les espace 
 * @brief cette fonction permet de prendre notre map decoupé
 * 
 * @param s 
 * @param delim 
 * @return vector<string> 
 */
vector<string> Gmap::explode(string const &s, char delim)
{
    vector<string> result;
    istringstream iss(s);
   
    
    for(string token;getline(iss, token, delim);)
    {
        result.push_back(move(token));


    }
    return result;
}


//chargement d un map depuis un fichier
/**
 * @brief permett de transformer une chaine de caractere et de le transformer en int 
 * @brief charge la map, et permet d enlever les espaces
 * 
 */
void Gmap::Transformchar_in_int() {
    ifstream ifs("map.txt");
    string contenue;
    if(ifs) {
        getline(ifs, contenue); 
        ifs.close();
    }


    
    auto exploded = explode (contenue, ' ');

    for(int i = 0;i < 450 ;i++){
        mapload[i] = stoi(exploded[i]); //Transforme une chaine de charactere en le tableau de int
    } 
}


/**
 * @brief gere le choix de la map
 * 
 * @param map 
 * @param choisis 
 * @return int 
 */
int Gmap::map_choisis( Map &map,int choisis)
{    
    
    Transformchar_in_int();

   if(choisis == 1){
        if (!map.load("data/tilesable.png", Vector2u(SPRITE_SIZE, SPRITE_SIZE), mapload, COL_COUNT, ROW_COUNT))
            return -1;
    } else if (choisis == 0){
        if (!map.load("data/test.jpg", Vector2u(SPRITE_SIZE, SPRITE_SIZE), mapload, COL_COUNT, ROW_COUNT))
            return -1;
    }
    return(0);
   
}

/**
 * @brief gere les evenements de la fenetre  , dessine la map 
 * 
 * @param window 
 * @param map 
 */
void Gmap::evenement_map(RenderWindow &window, Map &map)
{
      while (window.isOpen())
    {
        Event event; 
        // On boucle sur les �v�nements
        while (window.pollEvent (event)){
            if( event.type ==  Event::Closed){
                window.close();
     } 
        // Couleur de la fen�tre en noir
        window.clear(Color::Black);

        // C'est ici que l'on dessine les �l�ments du jeu
        window.draw(map);

        // Dessiner � l'�cran tous les �l�ments
        window.display();

    }
    }

}

/**
 * @brief gere les evenements de la map tel que la fermeture de la fenetre , appuie sur le bouton du bas pour descendre , 
 * @brief apuie sur le bouton du haut pour monter , appuie sur entrer pour passer a une nouvelle fenetre
 * @param window 
 * @param menu 
 * @param song 
 * @param choice_menu 
 */
void Gmap::gestionkey(RenderWindow &window,menu &menu,music &song ,bool &choice_menu)
{
    Event event;
        while(window.pollEvent(event)) {
            if(event.type == Event::Closed)
                window.close();
        
            if(event.type == Event::KeyPressed){
                if(event.key.code == Keyboard::Down)
                {
                   song.entermenu();
                    menu.moveDown();
                    break;
                }
                if(event.key.code == Keyboard::Up)
                {
                    song.entermenu();
                    menu.moveUp();
                    break;
                }
            //Foret
                if(event.key.code == Keyboard::Enter && menu.select() == 1 && choice_menu == 1) {
                     song.keymenu();
                     window.close();
                }
                //Desert
                if(event.key.code == Keyboard::Enter && menu.select() == 0 && choice_menu == 1) {
                    song.keymenu();
                    window.close();

                }
                //exit
                if(event.key.code == Keyboard::Enter && menu.select() == 1 && choice_menu == 0) {
                    window.close();
                }
                //play
                if(event.key.code == Keyboard::Enter && menu.select() == 0 && choice_menu == 0) {
                    song.keymenu();
                    menu.choixmap(960,640);
                    choice_menu = 1;

                }
            }
          
        }

}


/**
 * @brief 
 * 
 * @param choice 
 */
void Gmap::appelation_map(int choice)
{
    RenderWindow window;
    Map map;
    
    map_choisis( map,choice);
    evenement_map(window, map);
}

/**
 * @brief gere les evenement du menu, affiche le contenue du menu , permet de faire le gestion key 
 * 
 * @param window 
 * @param menu 
 * @param sprite 
 * @param song 
 * @param choice_menu 
 */
void Gmap::evenement_menu(RenderWindow &window, menu &menu,Sprite &sprite ,music &song, bool &choice_menu)
{
      while(window.isOpen()){
        gestionkey(window,menu,song,choice_menu);
        window.clear();
        window.draw(sprite);
        menu.draw(window);
        window.display();
      }

}

/**
 * @brief si choice menu ==1 on retourne la map choisit .
 * @return int 
 */
int Gmap::appelation_menu()
{
    RenderWindow window(VideoMode(960,640), "SFML_Make_menu");
    Texture image;
    Sprite sprite;
    image.loadFromFile("data/menu.jpg");
    sprite.setTexture(image);
    menu menu(960,640);
    music song;
    bool choice_menu = 0;

    
    evenement_menu(window,menu,sprite, song,choice_menu);
    
      if(choice_menu == 1)
        return(menu.select());
     return(-1);

}

