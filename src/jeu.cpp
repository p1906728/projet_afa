/**
 * @file jeu.cpp
 * @author AFA
 * @brief 
 * @version 0.1
 * @date 2022-05-15
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "jeu.h"

//enum des actions :directions
    enum DirHero1{Down1, Right1, Up1, Left1, Down_atk1, Right_atk1, Up_atk1, Left_atk1,atk_dis1};
    enum DirHero2{Down2, Right2, Up2, Left2, Down_atk2, Right_atk2, Up_atk2, Left_atk2,atk_dis2};
    Vector2f HeroAnim1(0,Down1);
    Vector2f HeroAnim2(0,Down2);


// creation 2 classe hero
     HERO hero1;
     HERO hero2;
    float a1 =400;
    float b1 =20;
    float a2 =400;
    float b2 =20;


/**
 * @brief la fonction ChecBtn1 permet aux personnage 1 de ce deplacer dans la map
 * 
 */
void HERO::CheckBtn1()
{
    if(heroidle1)
    {
        if(!needResetAnim1)
        {
            if (GetButton().left == true)
            {
            HeroAnim1.y = Left1;
            herosprite1.move(-SPEED,0);
            heroidle1 = false;
            }
        
            if (GetButton().right == true)
            {
            HeroAnim1.y = Right1;
            herosprite1.move(SPEED,0);
            heroidle1 = false;
            }

            if (GetButton().up == true)
            {
            HeroAnim1.y = Up1;
            herosprite1.move(0,-SPEED);
            heroidle1 = false;
            }

            if (GetButton().down == true)
            {
            HeroAnim1.y = Down1;
            herosprite1.move(0,SPEED);
            heroidle1 = false;
            }

            if (GetButton().attack == true)
            {
                needResetAnim1 =true;
                HeroAnim1.x = 0; // retourner a la col 0
                HeroAnim1.y += 4; // on  descend de 4 ligne sur notre sprite sheet

            }
            if(GetButton().atk_dis == true)
            {
                needResetAnim1 =true;
                HeroAnim1.x = 0; // retourner a la col 0
                HeroAnim1.y += 4; // on  descend de 4 ligne sur notre sprite sheet

                if(!bulletActive1)
                {
                    bulletActive1 = true;
                    bullet1.setPosition(herosprite1.getPosition().x +32, herosprite1.getPosition().y +32);
                    bullet1.setOrigin(32,32);   
                    bulletDir1 = HeroAnim1.y;    // heroanime.y = a l'une de ses valeur {Left1, Down_atk1, Right_atk1, Up_atk1, Left_atk1}        
                    bullet_clock1.restart();
                }

            }
        }

    }

    else
    {
        heroidle1 = true;
    }

    //les conditions pour que le herp ne sorte pas de la map
    if(herosprite1.getPosition().x <-15)
    {
        herosprite1.setPosition(-15,herosprite1.getPosition().y);
    }

    else if(herosprite1.getPosition().x > 915)
    {
        herosprite1.setPosition(915,herosprite1.getPosition().y);
    }
    else if((herosprite1.getPosition().y <20))
    {
        herosprite1.setPosition(herosprite1.getPosition().x,20);
    }
    else if(herosprite1.getPosition().y > 590)
    {
        herosprite1.setPosition(herosprite1.getPosition().x,590);
    }
    
}


/**
 * @brief la fonction ChecBtn1 permet aux personnage 2 de ce deplacer dans la map
 * 
 */
void HERO::CheckBtn2()
{
    if(heroidle2)
    {
        if(!needResetAnim2)
        {
            if (GetButton().left == true)
            {
            HeroAnim2.y = Left2;
            herosprite2.move(-SPEED,0);
            heroidle2 = false;
            }
        
            if (GetButton().right == true)
            {
            HeroAnim2.y = Right2;
            herosprite2.move(SPEED,0);
            heroidle2 = false;
            }

            if (GetButton().up == true)
            {
            HeroAnim2.y = Up2;
            herosprite2.move(0,-SPEED);
            heroidle2 = false;
            }

            if (GetButton().down == true)
            {
            HeroAnim2.y = Down2;
            herosprite2.move(0,SPEED);
            heroidle2 = false;
            }

            if (GetButton().attack == true)
            {
                needResetAnim2 =true;
                HeroAnim2.x = 0; // retourner a la col 0
                HeroAnim2.y += 4; // on  descend de 4 ligne sur notre sprite sheet

            }
            if(GetButton().atk_dis== true)
            {
                needResetAnim2 =true;
                HeroAnim2.x = 0; // retourner a la col 0
                HeroAnim2.y += 4; // on  descend de 4 ligne sur notre sprite sheet

                if(!bulletActive2)
                {
                    bulletActive2 = true;
                    bullet2.setPosition(herosprite2.getPosition().x +32, herosprite2.getPosition().y +32);
                    bullet2.setOrigin(32,32);   
                    bulletDir2 = HeroAnim2.y;    // heroanime.y = a l'une de ses valeur {Left1, Down_atk1, Right_atk1, Up_atk1, Left_atk1}        
                    bullet_clock2.restart();
                }

            }
        }

    }

    else
    {
        heroidle2 = true;
    }

    //les conditions pour que le herp ne sorte pas de la map
    if(herosprite2.getPosition().x <-15)
    {
        herosprite2.setPosition(-15,herosprite2.getPosition().y);
    }

    else if(herosprite2.getPosition().x > 915)
    {
        herosprite2.setPosition(915,herosprite2.getPosition().y);
    }
    else if((herosprite2.getPosition().y <20))
    {
        herosprite2.setPosition(herosprite2.getPosition().x,20);
    }
    else if(herosprite2.getPosition().y > 590)
    {
        herosprite2.setPosition(herosprite2.getPosition().x,590);
    }
    
}

/**
 * @brief gere l' animation du hero1 
 * 
 */
void HERO::AinimPlayer1()
{
    herosprite1.setTextureRect(IntRect(HeroAnim1.x*SPRITE,HeroAnim1.y*SPRITE,SPRITE,SPRITE));
    //animer en boucle la col (0->4)
    //si il s'est ecoupé 0.1s 
    if(hclock1.getElapsedTime().asSeconds() > 0.1f)
    {
     
        if(HeroAnim1.x*SPRITE >= herotexture1.getSize().x - SPRITE)
        {
            HeroAnim1.x = 0;
            // si l'animation de l'attaque tourne
            if(needResetAnim1)
            {
                //On l'arrete et on retourne sur l'anim de marche
                needResetAnim1= false;
                HeroAnim1.y -= 4;
            }
        }
        else
        {
            if(!heroidle1 || needResetAnim1)
             HeroAnim1.x++;
        }
        hclock1.restart();
    }
}

/**
 * @brief gere l' animation du hero1 , permet le deplacement 
 */
void HERO::AinimPlayer2()
{
    herosprite2.setTextureRect(IntRect(HeroAnim2.x*SPRITE,HeroAnim2.y*SPRITE,SPRITE,SPRITE));
    //animer en boucle la col (0->4)
    //si il s'est ecoupé 0.1s 
    if(hclock2.getElapsedTime().asSeconds() > 0.1f)
    {
       
        if(HeroAnim2.x*SPRITE >= herotexture2.getSize().x - SPRITE)
        {
            HeroAnim2.x = 0;
            // si l'animation de l'attaque tourne
            if(needResetAnim2)
            {
                //On l'arrete et on retourne sur l'anim de marche
                needResetAnim2= false;
                HeroAnim2.y -= 4;
            }
        }
        else
        {
            if(!heroidle2 || needResetAnim2) 
             HeroAnim2.x++;
        }
        hclock2.restart();
    }
}


/**
 * @brief charge l' image de l' hero 1
 */
void HERO::initHero1()
{
    if(!herotexture1.loadFromFile("data/hero_sheet.png"))
    cout<<"erreur chargement texture hero "<< endl;
    herosprite1.setTexture(herotexture1);
    hero1.herosprite1.setPosition(250,250);
}

/**
 * @brief charge l' image de l' hero 2
 */
void HERO::initHero2()
{
    if(!herotexture2.loadFromFile("data/hero_sheet.png"))
    cout<<"erreur chargement texture hero "<< endl;
    herosprite2.setTexture(herotexture2);
    hero2.herosprite2.setPosition(600,250);
}

/**
 * @brief charge l image de la projectile 1
 * 
 */
void HERO::initBullet1()
{
    bullet_textutre1.loadFromFile("data/atk.png");
    bullet1.setTexture(bullet_textutre1);
}

/**
 * @brief charge l image du bullet 
 * 
 */
void HERO::initBullet2()
{
    bullet_textutre2.loadFromFile("data/atk.png");
    bullet2.setTexture(bullet_textutre2);
}

 
/**
 * @brief @brief permet au personnage 1 de lancer un projectile 
 * @brief sous un temps données.
 * @param window 
 */
void HERO::HandleBullet1(RenderWindow& window)
{
    if(bulletActive1) // si on a tiré
    {
        switch (bulletDir1)
        {
        case Left_atk1:
            bullet1.setRotation(180);
            bullet1.move(-Bullet_SPEED,0);
            break;
        
        case Right_atk1:  
            bullet1.setRotation(0);
            bullet1.move(Bullet_SPEED,0);
            break;
        case Up_atk1:
            bullet1.setRotation(270);
            bullet1.move(0,-Bullet_SPEED);
            break;
        case Down_atk1:
            bullet1.setRotation(90);
            bullet1.move(0,Bullet_SPEED);
            break;
        default:
            break;
        }
        //affichage de lattaque
        window.draw(bullet1);

        // gerer la reinisialisation
        if(bullet_clock1.getElapsedTime().asSeconds() > 1.5f)
        {
            bulletActive1 =false;
        }
    }
}


/**
 * @brief permet au personnage 2 de lancer un projectile 
 * @brief sous un temps données.
 * @param window 
 */
void HERO::HandleBullet2(RenderWindow& window)
{
    if(bulletActive2) // si on a tiré
    {
        switch (bulletDir2)
        {
        case Left_atk2:
            bullet2.setRotation(180);
            bullet2.move(-Bullet_SPEED,0);
            break;
        
        case Right_atk2:
            bullet2.setRotation(0);
            bullet2.move(Bullet_SPEED,0);
            break;
        case Up_atk2:
            bullet2.setRotation(270);
            bullet2.move(0,-Bullet_SPEED);
            break;
        case Down_atk2:
            bullet2.setRotation(90);
            bullet2.move(0,Bullet_SPEED);
            break;
        default:
            break;
        }
        //affichage de lattaque
        window.draw(bullet2);

        // gerer la reinisialisation
        if(bullet_clock2.getElapsedTime().asSeconds() > 1.5f)
        {
            bulletActive2 =false;
        }
    }
   
}


/**
 * @brief cette fonction permet de faire la collision
 * @brief de permettre que les projectiles de feux ne traversent pas les personnages
 * @param window 
 */
void HERO::col(RenderWindow& window)
{

    /*RectangleShape cube(Vector2f(SPRITE,SPRITE));
    cube.setFillColor(Color(0,255,0,100));
    cube.setPosition(350,100);
    hitboxCUBE = cube.getGlobalBounds();
    window.draw(cube);*/

    bul1 = bullet1.getGlobalBounds();
    bul2 = bullet2.getGlobalBounds();
    hitbox_hero1 = herosprite1.getGlobalBounds();
    hitbox_hero2 = herosprite2.getGlobalBounds();

    /*RectangleShape h1(Vector2f(HeroAnim1.x+64,HeroAnim1.y+64));
    FloatRect h11;
    h1.setFillColor(Color::Blue);
    h1.setPosition(hero1.herosprite1.getPosition().x,hero1.herosprite1.getPosition().y);
    h11 = h1.getLocalBounds();
    window.draw(h1);*/

    RectangleShape lifeh1(Vector2f(a1,b1));
    lifeh1.setFillColor(Color::Blue);
   
    RectangleShape lifeh2(Vector2f(a2,b2));
    lifeh2.setFillColor(Color::Red);
    lifeh2.setPosition(WIN_WIDTH,b2);
    lifeh2.setRotation(180);
    window.draw(lifeh1);
    window.draw(lifeh2);

        if(hero2.hitbox_hero2.intersects(hero1.bul1) && bulletActive1)
        {
            bulletActive1=false;
            a2= a2-35;
        }
        else if(hero2.hitbox_hero2.intersects(hero1.hitbox_hero1) && hero1.GetButton().attack==true && needResetAnim1 )
        {   
           a2--;
        }
        
        else if(hero1.hitbox_hero1.intersects(hero2.bul2) && bulletActive2)
        {
            bulletActive2=false;
            a1= a1-35;
        }
        else if(hero1.hitbox_hero1.intersects(hero2.hitbox_hero2) && hero2.GetButton().attack==true && needResetAnim2)
        {
            a1--;
        }

    if((a1 <0) || (a2 <0))
    {
        hero1.herosprite1.setPosition(250,250);
        hero2.herosprite2.setPosition(600,250);
         a1 = a2 =400;
    }

       
}


void JEU::initHero()
{
    hero1.initHero1();
    hero2.initHero2();
    hero1.initBullet1();
    hero2.initBullet2();
}

void JEU::initboucle(){

    initHero();

    gmap.map_choisis(map,choice);

    
    while (window.isOpen())
    {
            
        Event event;
        while (window.pollEvent(event))
        {
            hero1.InputHandler1(event, window);
            hero2.InputHandler2(event, window);
        }
        hero1.AinimPlayer1();
        hero2.AinimPlayer2();
        window.draw(map);

    

        hero1.CheckBtn1();
        hero2.CheckBtn2();
        window.clear(Color::Black);
        window.draw(map);
        window.draw(hero1.herosprite1);
        window.draw(hero2.herosprite2);
        hero2.HandleBullet2(window);
        hero1.HandleBullet1(window);
        hero1.col(window);
        hero2.col(window);
        window.display(); 
       
        
    }
    
   
}
void JEU::initwindow()
{
    choice = gmap.appelation_menu();

    if(choice != - 1){
       window.create(VideoMode(WIN_WIDTH, WIN_HEIGHT, 32), "mon jeu sfml");
        // Activation du vsync
     window.setVerticalSyncEnabled(true);
    }
}

