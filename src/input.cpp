#include "input.h"
#include "jeu.h"
// Constructeur
/**
 * @brief Construct a new Input:: Input object
 * 
 */
Input::Input()
{
	button.left = button.right = button.up = button.down = button.attack = button.escape = button.atk_dis = false;
}

// Fonction qui accède à button (struc) et nous donne l'info private
/**
 * @brief Fonction qui accède à button (struc) et nous donne l'info private
 * 
 * @return Input::Button 
 */
Input::Button Input::GetButton(void) const 
{ 
	return button;
}

// Fonction de gestion des inputs
/**
 * @brief Fonction de gestion des boutons , verification s il sont appuyés ou relachés
 * 
 * @param event 
 * @param window 
 */
void Input::InputHandler1(Event event, RenderWindow& window)
{
    // Fermer la fenêtre si on clique sur la croix
    if (event.type == Event::Closed)
    {
        // On ferme la fenêtre
        window.close(); 
		
    }

    // Gestion des inputs (clavier / souris) 
    // Détection btn appuyé
    if (event.type == Event::KeyPressed)
    {
		switch (event.key.code) // Code de la touche utilisée
		{
		case Keyboard::Left:
			button.left = true;
			break;
		case Keyboard::Right:
			button.right = true;
			break;
		case Keyboard::Down:
			button.down = true;
			break;
		case Keyboard::Up:
			button.up = true;
			break;
		case Keyboard::M:
			button.attack = true;
			break;
		case Keyboard::P:
			button.atk_dis = true;
			break;
		default:
			break;
		}
    }
	// Test des key released
	if (event.type == Event::KeyReleased)
	{
		// Touche relâchée
		switch (event.key.code)
		{
		case Keyboard::Left:
			button.left = false;
			break;
		case Keyboard::Right:
			button.right = false;
			break;
		case Keyboard::Down:
			button.down = false;
			break;
		case Keyboard::Up:
			button.up = false;
			break;
		case Keyboard::M:
			button.attack = false;
			break;
		case Keyboard::P:
			button.atk_dis = false;
			break;
		default:
			break;
		}
	}
}

/**
 * @brief Fonction de gestion des boutons , verification s il sont appuyés ou relachés
 * 
 * @param event 
 * @param window 
 */
void Input::InputHandler2(Event event, RenderWindow& window)
{
    // Fermer la fenêtre si on clique sur la croix
    if (event.type == Event::Closed)
    {
        // On ferme la fenêtre
        window.close();
    }

    // Gestion des inputs (clavier / souris) 
    // Détection btn appuyé
    if (event.type == Event::KeyPressed)
    {
		switch (event.key.code) // Code de la touche utilisée
		{
		case Keyboard::Q:
			button.left = true;
			break;
		case Keyboard::D:
			button.right = true;
			break;
		case Keyboard::S:
			button.down = true;
			break;
		case Keyboard::Z:
			button.up = true;
			break;
		case Keyboard::A:
			button.attack = true;
			break;
		case Keyboard::E:
			button.atk_dis = true;
			break;
		default:
			break;
		}
    }
	// Test des key released
	if (event.type == Event::KeyReleased)
	{
		// Touche relâchée
		switch (event.key.code)
		{
		case Keyboard::Q:
			button.left = false;
			break;
		case Keyboard::D:
			button.right = false;
			break;
		case Keyboard::S:
			button.down = false;
			break;
		case Keyboard::Z:
			button.up = false;
			break;
		case Keyboard::A:
			button.attack = false;
			break;
		case Keyboard::E:
			button.atk_dis = false;
			break;
		default:
			break;
		}
	}
}