/**
 * @file menu.cpp
 * @author AFA
 * @brief 
 * @version 0.1
 * @date 2022-05-16
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "menu.h"
#include<SFML/Graphics.hpp>
#include <iostream>

/**
 * @brief charge le menu du jeu  avec play et exit
 * 
 * @param x 
 * @param y 
 */
menu::menu(float x, float y)
{

    //Play
    if (!font.loadFromFile("src/arial.ttf"))
    {
        cout<<"erreur"<<endl;
        exit(1);
    } 
    
    
            mainMenu[0].setFont(font);
            mainMenu[0].setFillColor(Color::White);
            mainMenu[0].setString("Play");
            mainMenu[0].setCharacterSize(24);
            mainMenu[0].setPosition(Vector2f(x/2, (y/2) - 100));
    

    //Exit
    if (!font.loadFromFile("src/arial.ttf")){
        cout<<"erreur"<<endl;
        exit(1);
         }
              mainMenu[1].setFont(font);
             mainMenu[1].setFillColor(Color::White);
             mainMenu[1].setString("Exit");
             mainMenu[1].setCharacterSize(24);
         mainMenu[1].setPosition(Vector2f(x/2, (y / 2)));
    mainMenuSelected = 0;
 }

/**
 * @brief gere le choix des map ,soit foret soit desert 
 * 
 * @param x 
 * @param y 
 */
void menu::choixmap(float x , float y)
 {
  
  //titre 
     if (!font.loadFromFile("src/arial.ttf"))
    {
        cout<<"erreur"<<endl;
        exit(1);
    } 

            m.setFont(font);
            m.setFillColor(Color::White);
            m.setString("CHOIX DE LA MAP");
            m.setCharacterSize(34);
            m.setPosition(Vector2f(x/2- 100, (y/2) - 200));
    
//Foret
    if (!font.loadFromFile("src/arial.ttf")){
        cout<<"erreur"<<endl;
        exit(1);
         }
              mainMenu[0].setFont(font);
             mainMenu[0].setFillColor(Color::White);
             mainMenu[0].setString("Foret");
             mainMenu[0].setCharacterSize(24);
         mainMenu[0].setPosition(Vector2f(x/2, (y / 2)- 100));
    mainMenuSelected = 0;

   //Desert
      if (!font.loadFromFile("src/arial.ttf")){
        cout<<"erreur"<<endl;
        exit(1);
         }
              mainMenu[1].setFont(font);
             mainMenu[1].setFillColor(Color::White);
             mainMenu[1].setString("Desert");
             mainMenu[1].setCharacterSize(24);
         mainMenu[1].setPosition(Vector2f(x/2, (y / 2)));
    mainMenuSelected = 0;


    
 }

//Afficher
/**
 * @brief permet de dessiner klle menu du play exit et le menu  du desert et foret
 * 
 * @param window 
 */
void menu::draw(RenderWindow &window)
{
    for (int i = 0; i <2; i++){
        window.draw(m);
        window.draw(mainMenu[i]);

    }
}

/**
 * @brief permet de descendre dans le menu et change la color initiale 
 * 
 */
void menu::moveDown()
{
    if(mainMenuSelected + 1<= 2) {

        mainMenu[mainMenuSelected].setFillColor(Color::White);
        mainMenuSelected++;

        if(mainMenuSelected == 2)
        {
            mainMenuSelected = 0;
        }
        mainMenu[mainMenuSelected].setFillColor(Color::Blue);

        
    }
}

/**
 * @brief permet de monter dans le menu et change la color initiale 
 * 
 */
void menu::moveUp()
{
    if(mainMenuSelected - 1>= -1){
        cout<<mainMenuSelected<<endl;
        mainMenu[mainMenuSelected].setFillColor(Color::White);
        mainMenuSelected --;
        if(mainMenuSelected == -1)
        {
            mainMenuSelected = 1;
        }
        mainMenu[mainMenuSelected].setFillColor(Color::Blue);
    }
        
}

/**
 * @brief fait le choix entre les menus
 * 
 * @return int 
 */
int menu::select()
{
    return(mainMenuSelected);

}

/**
 * @brief permet de modifier la valeur qu' on veut retourner
 * 
 * @param i 
 * @return int 
 */
int menu::select_exit(int i)
{
    int  mainMenuSelected = i;
    return(mainMenuSelected);
}