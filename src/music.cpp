/**
 * @file music.cpp
 * @author AFA
 * @brief 
 * @version 0.1
 * @date 2022-05-16
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include <SFML/Audio.hpp>
#include <SFML/Audio/Music.hpp>
#include "music.h"



using namespace sf; 


/**
 * @brief charge le son  pour le menu
 * 
 */
music::music()
{
      if(!song.openFromFile("data/song_menu.flac")){
           exit(-1);
    }
    song.setVolume(20.f);
    song.play();

}


/**
 * @brief charge le son pour le touche de clavier 
 * 
 */
void music::keymenu()
{
    if(!buffer.loadFromFile("data/soundeffectchoice.wav")){
        exit(-1);
    }
    sound.setBuffer(buffer);
    sound.play();
}


/**
 * @brief charge le son quant on appuie sur entrer
 * 
 */
void music::entermenu()
{
    if(!buffer.loadFromFile("data/soundeffectenter.wav")){
        exit(-1);
    }

    sound.setBuffer(buffer);
    sound.play();


}
