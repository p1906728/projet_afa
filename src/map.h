/**
 * @file map.h
 * @author AFA
 * @brief 
 * @version 0.1
 * @date 2022-05-16
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef MAP_H
#define MAP_H


#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Main.hpp>
#include <SFML/Config.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;
using namespace sf;


// Constantes du programme

/**
 * @brief prites de 32x32 pixels
 * 
 */
const int SPRITE_SIZE = 32; // On a des sprites de 32x32 pixels

/**
 * @brief Nombre de cases en largeur (col)
 * 
 */
const int COL_COUNT = 30; // Nombre de cases en largeur (col)

/**
 * @brief Nombre de cases en lignes (row)
 * 
 */
const int ROW_COUNT = 20; // Nombre de cases en lignes (row)



class Map : public Drawable, public Transformable
{
public:
    /**
     * @brief charge la map
     * 
     * @param tileset 
     * @param tileSize 
     * @param tiles 
     * @param width 
     * @param height 
     * @return true 
     * @return false 
     */
    bool load(const string& tileset, Vector2u tileSize, const int* tiles, 
        unsigned int width, unsigned int height);

    
    

private:
    /**
     * @brief dessine la map
     * 
     * @param target 
     * @param states 
     */
    virtual void draw(RenderTarget& target, RenderStates states) const
    {
        // on applique la transformation
        states.transform *= getTransform();

        // on applique la texture du tileset
        states.texture = &m_tileset;

        // et on dessine enfin le tableau de vertex
        target.draw(m_vertices, states);
    }

    VertexArray m_vertices;
    Texture m_tileset;
};

#endif