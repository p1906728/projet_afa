/**
 * @file menu.h
 * @author AFA
 * @brief 
 * @version 0.1
 * @date 2022-05-16
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Main.hpp>
#include <SFML/Config.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;
using namespace sf;

class menu
{
      public:
            bool choicewindow = 0;
            /**
             * @brief charge le menu avec une fentre de taille (x,y)
             * 
             * @param x 
             * @param y 
             */
            menu(float x, float y);

            /**
             * @brief dessine la map
             * 
             * @param window 
             */
            void draw(RenderWindow &window);
            
            /**
             * @brief gere l utilisatioln du bouton fleche bas  
             * 
             */
            void moveDown();

            /**
             * @brief gere l utilisatioln du bouton fleche haut  
             * 
             */
            void moveUp();
            
            /**
             * @brief select la map
             * 
             * @return int 
             */
            int select();
            
            /**
             * @brief charge la map de deser et de foret
             * 
             * @param x 
             * @param y 
             */
            void choixmap(float x, float y);

            /**
             * @brief ferme la fenetre
             * 
             * @param i 
             * @return int 
             */
            int select_exit(int i);
    private:
        int mainMenuSelected = 2;
        Font font;
        Text mainMenu[2];
        Text m;
      
};