#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Main.hpp>
#include <SFML/Config.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;
using namespace sf;


class music
{
      public:
            /**
             * @brief Charge le son du menu
             * 
             */
            music();

            /**
             * @brief charge le son pour les touches du clavier (fleche bas , fleche haut )
             * 
             */
            void keymenu();

            /**
             * @brief charge le son pour la touche du clavier (entrer) 
             * 
             */
            void entermenu();
      private:
            Music song;
            SoundBuffer buffer;
            Sound sound;
};