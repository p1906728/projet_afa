/**
 * @file jeu.h
 * @author AFA
 * @brief 
 * @version 0.1
 * @date 2022-05-12
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef MAIN_H
#define MAIN_H
// Includes
#include <iostream>
#include <SFML/Graphics.hpp>
#include "input.h"
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <utility>
#include "gestionmenu.h"
using namespace std;


// Namespaces
using namespace sf;
using namespace std;


/**
 * @brief la largeur
 */
const int WIN_WIDTH = 960;//25

/**
 * @brief lal longueur 
 * 
 */
const int WIN_HEIGHT = 640;//18

/**
 * @brief le sprite de l' hero 1
 */
const unsigned int SPRITE = 64;//sprite hero 1

/**
 * @brief la vitesse de l' heros
 */
const int SPEED = 10;

/**
 * @brief la vitesse de l arme utilisés
 */
const int Bullet_SPEED = 10;



class HERO : public Input 
{
   public: 
   //Preparation affichage perso 2D (hero1)
   Sprite herosprite1;
   Texture herotexture1;
   //Preparation affichage perso 2D (hero2)
   Sprite herosprite2;
   Texture herotexture2;
   //direction hero1 et 2
   enum DirHero1{Down1, Right1, Up1, Left1, Down_atk1, Right_atk1, Up_atk1, Left_atk1};
   enum DirHero2{Down2, Right2, Up2, Left2, Down_atk2, Right_atk2, Up_atk2, Left_atk2};
   
   //Preparation affichage des projectiles du hero1
   Sprite bullet1;
   Texture bullet_textutre1;
   bool bulletActive1 = false;
   int bulletDir1;
    //Preparation affichage des projectiles du hero2
   Sprite bullet2;
   Texture bullet_textutre2;
   bool bulletActive2 = false;
   int bulletDir2;

   //bool le perso est il a l'arret?
   bool heroidle1 = true;
   bool heroidle2 = true;
  
   //timer clock du hero 1
   Clock hclock1;
   Clock bullet_clock1;

   //timer clock du hero 2
   Clock hclock2;
   Clock bullet_clock2;
   //gestion de l'attaquec
   bool needResetAnim1 = false;
   bool needResetAnim2 = false;
   
   /**
    * @brief permet au personnage 1 de se deplacer et d attaque 
    * 
    */
   void CheckBtn1();
   
    /**
    * @brief permet au personnage 2 de se deplacer et d attaque 
    * 
    */
   void CheckBtn2();

   /**
    * @brief fait l' annimation du personnage 1
    * 
    */
   void AinimPlayer1();

   /**
    * @brief fait l' annimation du personnage 2
    * 
    */
   void AinimPlayer2();

   /**
    * @brief charge l image de l' heros 1
    * 
    */
   void initHero1();

   /**
    * @brief fait l' annimation du personnage 2
    * 
    */
   void initHero2();

   /**
    * @brief fait l' annimation de l' attaque de l' hero 1
    * @param window 
    */
   void HandleBullet1(RenderWindow& window);

    /**
    * @brief fait l' annimation de l' attaque de l' hero 2
    * @param window 
    */
   void HandleBullet2(RenderWindow& window);

   /**
    * @brief charge l' arme et projectile de l hero 1
    * 
    */
   void initBullet1();
   
    /**
    * @brief charge l' arme et projectile de l hero 1
    * 
    */
   void initBullet2();

   /**
    * @brief gere la collision entre les heros ou hero et bordure de la map
    * 
    * @param window 
    */
   void col(RenderWindow& window);
  
   FloatRect hitboxCUBE;
   FloatRect bul1;
   FloatRect bul2;
   FloatRect hitbox_hero2;
   FloatRect hitbox_hero1;
};

class  JEU : public HERO 
{

   public:
     RenderWindow window;
     Gmap gmap;
     Map map;
     int choice;
     /**
      * @brief gere l' annimation de la fenetre de jeu 
      * 
      */
   void initboucle();

   /**
    * @brief cree la fenetre de jeu
    * 
    */
   void initwindow();

   /**
    * @brief 
    * 
    */
   void initHero();
   
};

#endif