#ifndef INPUT_H
#define INPUT_H

/**
 * @file input.h
 * @author AFA
 * @brief 
 * @version 0.1
 * @date 2022-05-15
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include <SFML/Graphics.hpp>

using namespace sf;

class Input 
{
	/**
	 * @brief une structure pour les touches de bouton et les attack
	 * 
	 */
	struct Button { bool left, right, up, down, attack, escape, atk_dis;};
	

	public:
		// Proto du constructeur
		Input();
		// Proto GetBtn
		/**
		 * @brief Get the Button object
		 * 
		 * @return Button 
		 */
		Button GetButton(void) const;
		// Proto

		/**
		 * @brief gere les boutons de deplacement du hero1
		 * 
		 * @param event 
		 * @param window 
		 */
		void InputHandler1(Event event, RenderWindow& window);
		
		/**
		 * @brief gere les boutons de deplacement du hero2
		 * 
		 * @param event 
		 * @param window 
		 */
		void InputHandler2(Event event, RenderWindow& window);
	private:
		Button button;
	
};



#endif