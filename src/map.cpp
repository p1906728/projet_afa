/**
 * @file map.cpp
 * @author AFA
 * @brief 
 * @version 0.1
 * @date 2022-05-16
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "map.h"

/**
 * @brief charge la texture du tilset 
 * @brief on redimensionne le tableau de vertex pour qu'il puisse contenir tout le niveau
 * @brief remplit le tableau de vertex, avec un quad par tuile
 * 
 * @param tileset 
 * @param tileSize 
 * @param tiles 
 * @param width 
 * @param height 
 * @return true 
 * @return false 
 */
 bool Map::load(const string& tileset, Vector2u tileSize, const int* tiles, 
        unsigned int width, unsigned int height)
    {
        // on charge la texture du tileset
        if (!m_tileset.loadFromFile(tileset))
            return false;

        // on redimensionne le tableau de vertex pour qu'il puisse contenir tout le niveau
        m_vertices.setPrimitiveType(Quads);
        m_vertices.resize(width * height * 4);

        // on remplit le tableau de vertex, avec un quad par tuile
        for (unsigned int i = 0; i < width; ++i)
            for (unsigned int j = 0; j < height; ++j)
            {
                // on r�cup�re le num�ro de tuile courant
                int tileNumber = tiles[i + j * width];

                // on en d�duit sa position dans la texture du tileset
                int tu = tileNumber % (m_tileset.getSize().x / tileSize.x);
                int tv = tileNumber / (m_tileset.getSize().x / tileSize.x);

                // on r�cup�re un pointeur vers le quad � d�finir dans le tableau de vertex
                Vertex* quad = &m_vertices[(i + j * width) * 4];

                // on d�finit ses quatre coins
                quad[0].position = Vector2f(i * tileSize.x, j * tileSize.y);
                quad[1].position = Vector2f((i + 1) * tileSize.x, j * tileSize.y);
                quad[2].position = Vector2f((i + 1) * tileSize.x, (j + 1) * tileSize.y);
                quad[3].position = Vector2f(i * tileSize.x, (j + 1) * tileSize.y);

                // on d�finit ses quatre coordonn�es de texture
                quad[0].texCoords = Vector2f(tu * tileSize.x, tv * tileSize.y);
                quad[1].texCoords = Vector2f((tu + 1) * tileSize.x, tv * tileSize.y);
                quad[2].texCoords = Vector2f((tu + 1) * tileSize.x, (tv + 1) * tileSize.y);
                quad[3].texCoords = Vector2f(tu * tileSize.x, (tv + 1) * tileSize.y);
            }

        return true;
    }

    

