LITTLE FIGHTER 2D


Présentation du jeu

Lillte fighter est un jeux de combat en deux dimension inspiré du jeu Hero fighter , publié en 2009.

Information général :

Ce jeu a été crée par :


    • GUEYE Mouhamadou  Abdoulaye p2107928
    • HADDADJ Fares p1906728
    • Kacme-mhenni  Abdourrahmene p1914212



Règles du Jeu :

2 joueurs s’affrontent dans une map , le but est que l un des deux joueurs gagne la partie jusqu’à l’un des deux joueurs n’a plus de point de vie . Chaque personnage dispose de 400 pv  .

Foncionnalité :

Notre jeu dispose :


    • Un menue : Le menue dispose de deux choix possible , Il y’a le bouton Play qui nous renvoie sur la sélection des maps   et le bouton Exit qui permet de quitter la fenêtre donc le jeux. 
    • Écran du jeu : L’écran du jeu dispose de  la map sélectionner , les héros ainsi que les barres de vies

Compilation et exécution :

Le jeu est fait pour être compilé et exécuté essentiellement sous Kali linux  .

    • Pour installer SFML sur votre machine  Linux  , avec la commande : sudo apt-get install libsfml-dev
    • Pour compiler , on utilise un Makefile avec la commande : Make
    • Pour lancer le jeux en SFML : ./bin/main
    • Pour lancer le jeux en txt : ./bin/txt



Organisation :

A  la racine du projet se trouve le Makefile et le fichier map.txt :

    • Dans le dossier /bin , contient les exécutable 
    • Dans le dossier /data,contient les différent texture , music et police d’écriture
    • Dans le dossier /doc , contient la documentation doxygene
    • Dans le dossier /obj, contient les fichiers .o qui sont générer lors de la compilation
    • Dans le dossier /src, contient les fichiers .h et les fichiers .cpp


Génération de la documentation doxygene

Utiliser la commande $doxygene doc/documentation.doxy Puis ouvrir doc/html/index.html avec firefox.
